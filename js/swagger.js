/**
 * @file
 * Provides Swagger integration.
 */

(function ($, Drupal, drupalSettings) {

  // SwaggerUI expects $ to be defined as the jQuery object.
  // @todo File a patch to Swagger UI to not require this.
  window.$ = $;

  Drupal.behaviors.AJAX = {
    attach: function (context, settings) {
      var url = window.location.origin + drupalSettings.swagger.swagger_json_url;
      
      window.onload = function() {
        // Build a system
        const ui = SwaggerUIBundle({
          url: url,
          dom_id: '#swagger-ui',
          presets: [
            SwaggerUIBundle.presets.apis,
            SwaggerUIStandalonePreset
          ],
          plugins: [
            SwaggerUIBundle.plugins.DownloadUrl
          ],
          layout: "StandaloneLayout"
        })
      
        window.ui = ui
      }
    }
  };

})(jQuery, Drupal, drupalSettings);
